import React, { useState, useEffect } from 'react';
import './App.css'


  const route = {
     API_key: '095ae63791c5263e0ae3afd9c0fb478f',
     url: 'https://api.openweathermap.org/data/2.5/',
     proxy: 'https://cors-anywhere.herokuapp.com/'
  };


function App() {
  
  const [location, setLocation] = useState('');
  const [weather, setWeather] = useState({});

  const [ lat, setLat] = useState([]);
  const [ long, setLong] = useState([]);
  const [ data, setData] = useState([]);

  // getting the current location using gelocation
  useEffect(() => {
    navigator.geolocation.getCurrentPosition( function(position){
      setLat(position.coords.latitude);
      setLong(position.coords.longitude);
    });



    // lon: 121.0333, lat: 14.5833 ~~ mandaluyong
    // fetching the weather api to access the current location using latitude and longitude
    fetch(`${route.url}weather?lat=${lat}&lon=${long}&APPID=${route.API_key}&units=metric`)
    .then(res => res.json())
    .then(result => {
      setData(result)
 
    }).catch(err => err.message)



  // [lat, long] everytime the lat/long changes
  }, [lat, long])

  const searchLocation = (event) =>{

    if(event.key === "Enter"){
      // let location = event.target.value
      fetch(`${route.url}weather?q=${location}&APPID=${route.API_key}&units=metric`)
      .then(res => res.json())
      .then(data => {
        console.log(typeof data)
        setWeather(data);
        setLocation('');

        
      })
      .catch( err => console.log(err.message));
    };
  };



  // display the date today
  const dateToday = () =>{
    let date = String(new window.Date())
    return (date.slice(0,15))
  }

  return (
        <>
            <div className='container'>
              <div className="app-background"></div>
              <div className="container-glass">
                 {/*this is the current location of the page */}
                <div className="current-location">
                  <div className="title">
                    <div>
                      the.sky
                    </div>
                  </div>
                  <div className="timezone-info">
                  {(typeof data.main != 'undefined') ? (
                    <>

                      <div className="temp-current">
                        {data.main.temp} °C
                      </div>

                      <div className="loc-date">

                        <div className="loc-current">
                          {data.name}, {data.sys.country}
                        </div>
                        <div className="date-current">
                          {dateToday()}
                        </div>

                      </div>

                    </>
                  ) : ('')}
                  </div>
                </div>
                <div className="search-box">
                  <input type="text" 
                    className="search-bar city"
                    placeholder="Search your city..."
                    onChange={e => setLocation(e.target.value)}
                    value={location}
                    onKeyPress={searchLocation}
                  />
                
                  {(typeof weather.main != 'undefined') ? (

                      <div className="input-location">

                        <div className="location">
                          {weather.name}, {weather.sys.country}
                        </div>

                        <div className="weather-details">

                          <div>
                            <p>{weather.weather[0].description}</p>
                            <img src={`http://openweathermap.org/img/wn/${weather.weather[0].icon}.png`} alt="icon" />
                          </div>
                          <div>
                            <p>Temperature</p>
                            <span>{weather.main.temp} °C</span>
                          </div>
                          
                          <div>
                            <p>Humidity</p>
                            <span>68%</span>
                          </div>
                          <div>
                            <p>Clouds</p>
                            <span>98%</span>
                          </div>
                          <div>
                            <p>Wind</p>
                            <span>{weather.wind.speed} km/h</span>
                          </div>

                        </div>

                      </div>

                  ) : ('')}
                </div>
              </div>
            </div>

        </>

  );
}
export default App;


                  
